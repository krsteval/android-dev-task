# Task #

Please implement the following UI as close to design as possible.

### What is this repository for? ###

* Quick summary
* Version 1.0.0
* [Krste Moskov]

### Project set up ###

* minSdkVersion = 15
* targetSdkVersion = 26
* compileSdkVersion = 26

* compileOptions = 1.8  - Use Java 8 Language Features

```
* If you use lower version of Android Studio 3.0.1, you can set up and jackOptions 
jackOptions {
	enabled true
	...
}
```

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Dependencies ###
```
* Its default local variable for all library from android SDK 
def SUPPORT = "26.1.0"

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])

    compile "com.android.support:design:$SUPPORT"
    compile "com.android.support:appcompat-v7:$SUPPORT"

    compile "com.android.support:cardview-v7:$SUPPORT"
    compile "com.android.support:recyclerview-v7:$SUPPORT"

    implementation 'com.android.support.constraint:constraint-layout:1.0.2'

    //Butter Knife - Field and method binding for Android views
    compile 'com.jakewharton:butterknife:8.8.1'
    annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'

    // Expandable Layout
    compile 'com.github.aakira:expandable-layout:1.6.0@aar'

    // Circle ImageView
    compile 'de.hdodenhof:circleimageview:2.2.0'
 
}
```
### File structure ###
```

├── com.moskov.krste.android_dev_task
│   ├── activities
│   │   ├── indexRouter.js - Default rute response config 
│   ├── adapters
│   │   ├── RecyclerViewAdapter_EventsCleaner.java - binding events in recycler view 
│   │   ├── RecyclerViewAdapter_EventDetails.java - binding details for one event in recycler view  
│   ├── helpers
│   │   ├── Common.java - singleton class, sharing list of event cleaners  
│   ├── models
│   │   ├── EventCleaner.java - Base model for application
│   │   ├── EventDetails.java - model especially for showing details about one event
│   ├── MainActivity.java - initial class from empty solution, it's not on use for project
│   ├── SplashActivity.java - startup activity for setting data simulate simple splash screen when insert dummy data


```
### User Model ###
```
#EventCleaner

public class EventCleaner {
    public enum EventType {
        Cafeteria,
        Wommen_Bethroom,
        Men_Bethroom,
        Meeting_Room;

        static EventType fromTypeString(String type) {
            for (EventType eventType : EventCleaner.EventType.values()) {
                if (eventType.name().equalsIgnoreCase(type)) {
                    return eventType;
                }
            }
            return null;
        }
    }

    private Integer floor;
    private String eventName;
    private String eventType;
    private boolean isWarning;
    private ArrayList<EventDetails> eventDetailsList;

	. . .
}

#EventDetails
public class EventDetails {
    private String name;
    private Integer icons;
    private boolean isWarning;
	
	. . .
}
```

### Local Data Items ###

```
new EventDetails
(
	"Paper towel", 
	R.mipmap.ic_towels, 
	true
)


new EventCleaner
(
	"Women Bathroom",
	21,
	EventCleaner.EventType.Wommen_Bethroom.name(),
	true,
	eventDetails
)
```