package com.moskov.krste.android_dev_task.models;

/**
 * Created by krste on 06.3.2018.
 */

public class EventDetails {
    private String name;
    private Integer icons;
    private boolean isWarning;

    public EventDetails(String name, Integer icons, boolean isWarning) {
        this.name = name;
        this.icons = icons;
        this.isWarning = isWarning;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIcons() {
        return icons;
    }

    public void setIcons(Integer icons) {
        this.icons = icons;
    }

    public boolean isWarning() {
        return isWarning;
    }

    public void setWarning(boolean warning) {
        isWarning = warning;
    }
}
