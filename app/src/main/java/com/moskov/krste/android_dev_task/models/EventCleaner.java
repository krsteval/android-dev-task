package com.moskov.krste.android_dev_task.models;

import java.util.ArrayList;

/**
 * Created by krste on 05.3.2018.
 */

public class EventCleaner {
    public enum EventType {
        Cafeteria,
        Wommen_Bethroom,
        Men_Bethroom,
        Meeting_Room;

        static EventType fromTypeString(String type) {
            for (EventType eventType : EventCleaner.EventType.values()) {
                if (eventType.name().equalsIgnoreCase(type)) {
                    return eventType;
                }
            }
            return null;
        }
    }

    private Integer floor;
    private String eventName;
    private String eventType;
    private boolean isWarning;
    private ArrayList<EventDetails> eventDetailsList;


    public EventCleaner(String eventName, Integer floor, String eventType, boolean isWarning, ArrayList<EventDetails> eventDetailsList) {
        this.floor = floor;
        this.eventName = eventName;
        this.eventType = eventType;
        this.isWarning = isWarning;
        this.eventDetailsList = eventDetailsList;
    }


    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public EventType getStatus() {
        return EventType.fromTypeString(eventType);
    }

    public void setStatus(EventType eventType) {
        this.eventType = eventType.name();
    }

    public boolean isWarning() {
        return isWarning;
    }

    public void setWarning(boolean warning) {
        isWarning = warning;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public ArrayList<EventDetails> getEventDetailsList() {
        return eventDetailsList;
    }

    public void setEventDetailsList(ArrayList<EventDetails> eventDetailsList) {
        this.eventDetailsList = eventDetailsList;
    }
}
