package com.moskov.krste.android_dev_task.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.moskov.krste.android_dev_task.R;
import com.moskov.krste.android_dev_task.helpers.Common;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.image_go_back)
    ImageView image_go_back;

    @BindView(R.id.text_event_floor_no)
    TextView text_event_floor;
    @BindView(R.id.text_event_type)
    TextView text_event_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);
        setComponents();
    }

    private void setComponents() {

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            Integer header = bundle.getInt("TAG_EVENT_POSITION");
            text_event_floor.setText(Common.getInstance().listEventCleaner.get(header).getFloor() < 10 ? "0" + Common.getInstance().listEventCleaner.get(header).getFloor() : Common.getInstance().listEventCleaner.get(header).getFloor().toString());
            text_event_type.setText(Common.getInstance().listEventCleaner.get(header).getEventName());
        }
        image_go_back.setOnClickListener(view->{
            onBackPressed();
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
