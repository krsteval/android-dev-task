package com.moskov.krste.android_dev_task.helpers;

import com.moskov.krste.android_dev_task.models.EventCleaner;

import java.util.ArrayList;

/**
 * Created by krste on 06.3.2018.
 */

public class Common {
    private static final Common ourInstance = new Common();

    public static Common getInstance() {
        return ourInstance;
    }

    private Common() {
    }

    public ArrayList<EventCleaner> listEventCleaner;
}
