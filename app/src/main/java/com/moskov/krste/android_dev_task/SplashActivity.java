package com.moskov.krste.android_dev_task;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.moskov.krste.android_dev_task.activities.StartScanActivity;
import com.moskov.krste.android_dev_task.helpers.Common;
import com.moskov.krste.android_dev_task.models.EventCleaner;
import com.moskov.krste.android_dev_task.models.EventDetails;

import java.util.ArrayList;


/**
 * Created by krste on 04.3.2018.
 */

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spash_screen);

        initDamyData();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                Intent mainIntent = new Intent(SplashActivity.this, StartScanActivity.class);
                startActivity(mainIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void initDamyData() {
        Common.getInstance().listEventCleaner = new ArrayList<>();

        // Populate details for Event
        ArrayList<EventDetails> eventDetails = new ArrayList<>();
        eventDetails.add(new EventDetails("Paper towel", R.mipmap.ic_towels, true));
        eventDetails.add(new EventDetails("Dishes", R.mipmap.ic_dishes, true));
        eventDetails.add(new EventDetails("Trash", R.mipmap.trash, false));
        eventDetails.add(new EventDetails("Floor", R.mipmap.floor, false));
        eventDetails.add(new EventDetails("Task 5", R.mipmap.task, false));
        eventDetails.add(new EventDetails("Task 6", R.mipmap.task, false));
        eventDetails.add(new EventDetails("Task 7", R.mipmap.task, false));
        eventDetails.add(new EventDetails("Task 8", R.mipmap.task, false));

        //add list of Events
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Women Bathroom",
                        21,
                        EventCleaner.EventType.Wommen_Bethroom.name(),
                        true,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Cafeteria",
                        6,
                        EventCleaner.EventType.Cafeteria.name(),
                        true,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Meeting room",
                        4,
                        EventCleaner.EventType.Meeting_Room.name(),
                        false,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Men Bathroom",
                        8,
                        EventCleaner.EventType.Men_Bethroom.name(),
                        false,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Cafeteria",
                        12,
                        EventCleaner.EventType.Cafeteria.name(),
                        false,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Women Bathroom",
                        16,
                        EventCleaner.EventType.Wommen_Bethroom.name(),
                        false,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Meeting room",
                        26,
                        EventCleaner.EventType.Meeting_Room.name(),
                        false,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Men Bathroom",
                        18,
                        EventCleaner.EventType.Men_Bethroom.name(),
                        false,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Meeting room",
                        2,
                        EventCleaner.EventType.Meeting_Room.name(),
                        true,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Cafeteria",
                        9,
                        EventCleaner.EventType.Cafeteria.name(),
                        false,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Women Bathroom",
                        36,
                        EventCleaner.EventType.Wommen_Bethroom.name(),
                        false,
                        eventDetails
                )
        );
        Common.getInstance().listEventCleaner.add(
                new EventCleaner(
                        "Meeting room",
                        22,
                        EventCleaner.EventType.Meeting_Room.name(),
                        false,
                        eventDetails
                )
        );

    }
}
