package com.moskov.krste.android_dev_task.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.moskov.krste.android_dev_task.R;
import com.moskov.krste.android_dev_task.models.EventDetails;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krste on 05.3.2018.
 */

public class RecyclerViewAdapter_EventDetails extends RecyclerView.Adapter<RecyclerViewAdapter_EventDetails.EventViewHolder> {
    private List<EventDetails> events;
    private Context context;

    public class EventViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_event_detail_title)
        TextView text_event_detail_title;


        @BindView(R.id.image_event_detail_icon)
        ImageView image_event_detail_icon;


        EventViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);

            text_event_detail_title.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/roboto_regular.ttf"));

        }

    }

    public RecyclerViewAdapter_EventDetails(Context context, List<EventDetails> events) {

        this.context = context;
        this.events = events;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new EventViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_details, parent, false));
    }


    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.text_event_detail_title.setText(events.get(position).getName());
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.image_event_detail_icon.setImageDrawable(context.getResources().getDrawable(events.get(position).getIcons(), context.getTheme()));
            } else {
                holder.image_event_detail_icon.setImageDrawable(context.getResources().getDrawable(events.get(position).getIcons()));
            }
            if(events.get(position).isWarning()){
                holder.image_event_detail_icon.setColorFilter(context.getResources().getColor(R.color.colorTaskDanger));
                holder.text_event_detail_title.setTextColor(context.getResources().getColor(R.color.colorTaskDanger));
            }else
            {
//                holder.image_event_detail_icon.setColorFilter(context.getResources().getColor(R.color.colorHint));
                holder.text_event_detail_title.setTextColor(context.getResources().getColor(R.color.text_event_name));
            }
        } catch (Exception ex) {
            Log.i("Error" , ex.toString());
        }
    }

    @Override
    public int getItemCount() {
        Log.d("COUNT", "" + events.size());
        return events == null ? 0 : events.size();

    }
}