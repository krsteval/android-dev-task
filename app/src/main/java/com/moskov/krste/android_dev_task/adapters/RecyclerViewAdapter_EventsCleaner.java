package com.moskov.krste.android_dev_task.adapters;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;
import com.moskov.krste.android_dev_task.R;
import com.moskov.krste.android_dev_task.activities.DetailsActivity;
import com.moskov.krste.android_dev_task.models.EventCleaner;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krste on 05.3.2018.
 */

public class RecyclerViewAdapter_EventsCleaner extends RecyclerView.Adapter<RecyclerViewAdapter_EventsCleaner.EventViewHolder> {
    private List<EventCleaner> events;
    private Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();

    private RecyclerViewAdapter_EventDetails adapter_eventDetails;

    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.text_event_name)
        TextView text_event_name;
        @BindView(R.id.text_number_of_floor)
        TextView text_number_of_floor;
        @BindView(R.id.text_event_floor_title)
        TextView text_event_floor_title;

        @BindView(R.id.image_event_type)
        ImageView image_event_type_bg;
        @BindView(R.id.image_down_show_details)
        ImageView image_down_details;
        @BindView(R.id.image_event_type_icon)
        ImageView image_event_type_icon;

        @BindView(R.id.expandable_linear_layout_view_event)
        ExpandableLinearLayout expandable_linear_layout_view_event;

        @BindView(R.id.recycler_view_event_details)
        RecyclerView recycler_view_event_details;

        @BindView(R.id.tmpLayoutItems)
        LinearLayout layoutTmp;
        @BindView(R.id.tmpLayoutItemsSecond)
        LinearLayout layoutTmpSecond;

        EventViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);
            v.setOnClickListener(this);

            text_event_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/roboto_bold.ttf"));
            text_number_of_floor.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/roboto_regular.ttf"));
            text_number_of_floor.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/roboto_medium.ttf"));

            expandable_linear_layout_view_event.initLayout();
            recycler_view_event_details.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

                @Override
                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                    int action = e.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_MOVE:
                            rv.getParent().requestDisallowInterceptTouchEvent(true);
                            break;
                    }
                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            });

//            recycler_view_event_details.setHasFixedSize(true);
//            recycler_view_event_details.setLayoutManager(
//                    new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//            );
        }

        @Override
        public void onClick(View view) {

            Integer position = getAdapterPosition();

//            Toast.makeText(context, "Click: " + events.get(position).getEventName(), Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(context, DetailsActivity.class);
////            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intent.putExtra("TAG_EVENT_POSITION", position);
//            context.startActivity(intent);
        }
    }

    public RecyclerViewAdapter_EventsCleaner(List<EventCleaner> events, Context context) {

        this.events = events;
        this.context = context;
        for (int i = 0; i < events.size(); i++) {
            expandState.append(i, false);
        }
    }

    @Override
    public EventViewHolder  onCreateViewHolder(ViewGroup parent, int viewType) {

        return new EventViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent, false));
    }


    @Override
    public void onBindViewHolder(EventViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final EventCleaner eventCleaner = events.get(position);
        holder.setIsRecyclable(false);
        holder.text_event_name.setText(eventCleaner.getEventName());
        holder.text_event_floor_title.setText(eventCleaner.getFloor() < 10 ? "Floor 0" + eventCleaner.getFloor().toString() : "Floor " + eventCleaner.getFloor().toString());
        holder.text_number_of_floor.setText(eventCleaner.getFloor() < 10 ? "0" + eventCleaner.getFloor().toString() : eventCleaner.getFloor().toString());
        int image = R.mipmap.ic_dishes;
        if(eventCleaner.getEventType().equals(EventCleaner.EventType.Meeting_Room.name())){
            image = R.drawable.ic_meeting;
        }
        else if(eventCleaner.getEventType().equals(EventCleaner.EventType.Men_Bethroom.name())){
            image = R.drawable.ic_men;
        }
        else if(eventCleaner.getEventType().equals(EventCleaner.EventType.Wommen_Bethroom.name())){
            image = R.drawable.ic_women;
        }
        else if(eventCleaner.getEventType().equals(EventCleaner.EventType.Cafeteria.name())){
            image = R.drawable.ic_cafeteria;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.image_event_type_icon.setImageDrawable(context.getResources().getDrawable(image, context.getApplicationContext().getTheme()));
        } else {
            holder.image_event_type_icon.setImageDrawable(context.getResources().getDrawable(image));
        }

        if(events.get(position).isWarning()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.image_event_type_bg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_box_danger, context.getApplicationContext().getTheme()));
            }
            else{
                holder.image_event_type_bg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_box_danger));
            }

            holder.text_number_of_floor.setTextColor(context.getResources().getColor(R.color.backgroundTaskDanger));
        }else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.image_event_type_bg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_box, context.getApplicationContext().getTheme()));
            }else{

                holder.image_event_type_bg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_box));
            }
            holder.text_number_of_floor.setTextColor(context.getResources().getColor(R.color.trekeye_blue));
        }
//        if(holder.expandable_linear_layout_view_event.isExpanded())
//            holder.expandable_linear_layout_view_event.collapse();
//        holder.expandable_linear_layout_view_event.toggle();
        holder.expandable_linear_layout_view_event.setInRecyclerView(true);
        holder.expandable_linear_layout_view_event.setExpanded(expandState.get(position));
        holder.expandable_linear_layout_view_event.setListener(new ExpandableLayoutListener(holder, position));

//        holder.image_down_details.setRotation(expandState.get(position) ? 180f : 0f);
        holder.image_down_details.setOnClickListener(view-> onClickButton(holder.expandable_linear_layout_view_event));
    }
 
    public class ExpandableLayoutListener extends ExpandableLayoutListenerAdapter {

        EventViewHolder holder;
        int position;

        private ExpandableLayoutListener(EventViewHolder holder, int position) {
            this.holder = holder;
            this.position = position;
        }

        @Override
        public void onPreOpen() {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.image_down_details.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_down_copy_3, context.getApplicationContext().getTheme()));
            } else {
                holder.image_down_details.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_down_copy_3));
            }
//                createRotateAnimator(holder.image_down_details, 0f, 180f).start();
            expandState.put(position, true);
            holder.recycler_view_event_details.setHasFixedSize(true);
            holder.recycler_view_event_details.setLayoutManager(new LinearLayoutManager(holder.recycler_view_event_details.getContext()));
            // NOTE: need to disable change animations to ripple effect work properly
            ((SimpleItemAnimator) holder.recycler_view_event_details.getItemAnimator()).setSupportsChangeAnimations(false);
            holder.expandable_linear_layout_view_event.setInRecyclerView(true);
            adapter_eventDetails = new RecyclerViewAdapter_EventDetails(context, events.get(position).getEventDetailsList());
            holder.recycler_view_event_details.setAdapter(adapter_eventDetails);


            holder.layoutTmp.setVisibility(View.GONE);
            holder.layoutTmpSecond.setVisibility(View.GONE);
        }

        @Override
        public void onPreClose() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.image_down_details.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_down_copy_2, context.getApplicationContext().getTheme()));
            } else {
                holder.image_down_details.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_down_copy_2));
            }
//                createRotateAnimator(holder.image_down_details, 180f, 0f).start();
            expandState.put(position, false);
        }
    }
    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
    }
    @Override
    public int getItemCount() {
        return events == null ? 0 : events.size();

    }

    private ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}