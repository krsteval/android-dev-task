package com.moskov.krste.android_dev_task.activities;

/**
 * Created by krste on 06.3.2018.
 */

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.moskov.krste.android_dev_task.R;
import com.moskov.krste.android_dev_task.adapters.RecyclerViewAdapter_EventsCleaner;
import com.moskov.krste.android_dev_task.helpers.Common;


import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("Registered")
public class StartScanActivity extends AppCompatActivity
{
    private static final int PERCENTAGE_TO_SHOW_IMAGE = 40;
    private int mMaxScrollSize;
    private boolean mIsImageHidden;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    @BindView(R.id.linear_layout_start_scan)
    LinearLayout linear_layout_start_scan;
    @BindView(R.id.linear_layout_partial_main_scaning_results)
    LinearLayout linear_layout_partial_main_scaning_results;



    @BindView(R.id.frame_start_scan_visible)
    FrameLayout frame_start_scan_visible;
//    @BindView(R.id.frame_start_scan_collapse)
//    FrameLayout frame_start_scan_collapse;

    @BindView(R.id.listScranRes)
    RecyclerView recyclerView;

    @BindView(R.id.text_no_current_mission)
    TextView text_no_current_mission;
    @BindView(R.id.text_tab_to_start)
    TextView text_tab_to_start;

    Typeface roboto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_scan);
        ButterKnife.bind(this);

        initComponents();
        setComponents();
    }

    private void initComponents() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private void setComponents() {

        text_no_current_mission.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/roboto_bold.ttf"));
        text_tab_to_start.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/roboto_bold.ttf"));
//        appbar.addOnOffsetChangedListener(this);


        frame_start_scan_visible.setOnClickListener(view->{
            text_no_current_mission.setVisibility(View.GONE);
            linear_layout_partial_main_scaning_results.setVisibility(View.VISIBLE);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false)
            );
            RecyclerViewAdapter_EventsCleaner adapter = new RecyclerViewAdapter_EventsCleaner( Common.getInstance().listEventCleaner, this);
            recyclerView.setAdapter(adapter);
        });
    }

}
